$(function() {
  var $s = $('.full-page.swipe');
  if ($s.length == 0)
    return;

  //so that changing subviews = hash change doesn't insert junk into history
  $('.page-head .unstyled a').on('click', function() {
    window.location.replace($(this).attr('href'));
    return false;
  });

  //init swipe
  var hash = window.location.hash,
     start = hash ? $('[data-page=' + hash.substring(1) + ']').prevAll().length : 0,
   current = start;

  var s = Swipe($s[0], {
    startSlide: start,
    callback: function(i,e) {
      //while page is sliding, also slide subnav
      updateSubnav(i);
    }
  });

  //updating the subnav
  var $ol = $('.page-head .unstyled'),
      $li = $('.page-head li'),
    liMap = {};

  $li.each(function(i,e) { liMap[i] = $(e); });

  function updateSubnav(i, force) {
    //we only need current to determine direction so
    //once we're done with it, we should update current
    //as soon as possible so that subnav can update 
    //correctly even if user swipes "too fast"
    var direction = i - current;
    current = i;

    $li.removeClass('current');
    liMap[i].addClass('current');

    //slide effect
    if (!force && direction == 0) //no change
      return;

    if (direction < -1)
      direction += s.getNumSlides();

    var rotate = {
      back: function() {    // <--
        $ol.find('li:first').detach().appendTo($ol);
      },
      forward: function() { // -->
        $ol.find('li:last').detach().prependTo($ol);
      }
    };

    var fn = direction > 0 ? 'back' : 'forward';
    do { rotate[fn](); }
    while(liMap[i].prevAll().length);
  };

  //on hash change, swipe to correct page
  $(window).on('hashchange', function() {
    var hash = window.location.hash;
    if (hash) {
      var $div = $('[data-page=' + hash.substring(1) + ']');
      if ($div.length > 0) {
        s.slide($div.data('index'));
      }
    }
  });

  //start page
  updateSubnav(start, true);

  //debugging
  //window.s = s; window.$s = $s;
});