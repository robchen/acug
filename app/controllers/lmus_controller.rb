class LmusController < ApplicationController
  before_action :set_lmu, only: [:show, :edit, :update, :destroy]

  # GET /lmus
  # GET /lmus.json
  def index
    @lmus = Lmu.all

    #set defaults
    @lmu = Lmu.new

    upgrade = @lmu.build_upgrade

    flight = upgrade.build_flight
    flight.date = Date.current
  end

  # GET /lmus/1
  # GET /lmus/1.json
  def show
  end

  # GET /lmus/1/edit
  def edit
  end

  # POST /lmus
  # POST /lmus.json
  def create
    @lmu = Lmu.new(lmu_params)

    respond_to do |format|
      if @lmu.save
        format.html { redirect_to @lmu, notice: 'Lmu was successfully created.' }
        format.json { render action: 'show', status: :created, location: @lmu }
      else
        format.html { render action: 'new' }
        format.json { render json: @lmu.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /lmus/1
  # PATCH/PUT /lmus/1.json
  def update
    respond_to do |format|
      if @lmu.update(lmu_params)
        format.html { redirect_to @lmu, notice: 'Lmu was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @lmu.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /lmus/1
  # DELETE /lmus/1.json
  def destroy
    @lmu.destroy
    respond_to do |format|
      format.html { redirect_to lmus_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_lmu
      @lmu = Lmu.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def lmu_params
      params.require(:lmu).permit(
        :price,
        :currency_id,
        :purchased,
        upgrade_attributes: [
          :status_id,
          :cabin_id,
          flight_attributes: [
            :date,
            :number,
            :from_id,
            :to_id,
            :airplane_id,
            :fare
          ]
        ]
      )
    end
end
