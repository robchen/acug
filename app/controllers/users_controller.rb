class UsersController < ApplicationController
  before_action :set_user, only: [:verify, :update, :destroy]

  # GET /users
  def show
  end

  # GET /users/login
  def login
  end

  # POST /users/logout
  def logout
    if request.post?
      reset_session
      redirect_to :root
    end
  end

  # GET /users/verify
  def verify
    if @user.present?
      session['user_id'] = @user.id
      redirect_to :action => 'show'
    else
      flash[:error] = "Bad username/password"
      redirect_to :action => 'login'
    end
  end

  # GET /users/new
  def new
    @user = User.new
  end

  # POST /users
  def create
    @user = User.new(user_params)
    @user.needs_admin = true

    if @user.save
      redirect_to new_users_url, notice: 'User was successfully created.'
    else
      render action: 'new'
    end
  end

  # PATCH/PUT /users
  def update
    if @user.update(user_params)
      redirect_to users_url, notice: 'Update successful'
    else
      render action: 'edit'
    end
  end

  # DELETE /users
  def destroy
    @user.needs_admin = true

    @user.destroy
    redirect_to users_url, notice: 'User was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find_by_input(user_params)
    end

    # Only allow a trusted parameter "white list" through.
    def user_params
      params.require(:user).permit(
        :username,
        :password,
        :admin_password
      )
    end
end
