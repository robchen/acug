class Upgrade < ActiveRecord::Base
  belongs_to :flight
  belongs_to :upgradeable, :polymorphic => true

  accepts_nested_attributes_for :flight

  STATUS = {
    se: 0,
    a75: 1,
    a50: 2,
    a35: 3,
    a25: 4,
    e: 5,
    p: 6,
    none: 7,
    vip: 8
  }

  STATUS_NAMES = {
    se: 'Super Elite',
    a75: 'Elite 75K',
    a50: 'Elite 50K',
    a35: 'Elite 35K',
    a25: 'Prestige 25K',
    e: 'Elite (pre-Altitude)',
    p: 'Prestige (pre-Altitude)',
    none: 'No Status',
    vip: 'VIP'
  }

  def status
    STATUS.key(read_attribute(:status_id))
  end

  def status=(s)
    write_attribute(:status_id, STATUS[s])
  end

  CABIN = {
    executive_first: 0,
    executive: 1,
    premium_economy: 2
  }

  CABIN_NAMES = {
    executive_first: 'Executive First',
    executive: 'Executive Class',
    premium_economy: 'Premium Economy'
  }

  def cabin
    CABIN.key(read_attribute(:cabin_id))
  end

  def cabin=(s)
    write_attribute(:cabin_id, CABIN[s])
  end
end
