class User < ActiveRecord::Base
  validates_presence_of :username, :password
  validates_uniqueness_of :username, :case_sensitive => false

  attr_accessor :admin_password, :needs_admin
  before_validation :check_admin

  def self.find_by_input(hash)
    user = User.find_by_username(hash[:username])

    if user
      salted_pass = hash[:password] + APP_SECRET['password_salt']
      sha256_hash = Digest::SHA256.hexdigest(salted_pass)

      return user if ((user.password == sha256_hash) or
                      (hash[:admin_password].present? and correct_admin_password?(hash[:admin_password])))
    end
  end

  private

  def correct_admin_password?(password)
    password == APP_SECRET['super_admin_password']
  end

  def check_admin
    needs_admin ? correct_admin_password?(admin_password) : true
  end
end
