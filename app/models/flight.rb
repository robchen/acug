class Flight < ActiveRecord::Base
  belongs_to :airplane
  belongs_to :from, :class_name => City
  belongs_to :to,   :class_name => City

  #domestic
  #tango: NGPEA
  #flex:  MUHQVWSTLK
  #lat:   YB
  #exec:  JCDZ
  #award: X

  #international
  #note: TLK is tango here, but is flex for domestic
  #
  #tango: NGPEATLK
  #flex:  MUHQVWS
  #lat:   YB
  #prem:  O
  #exec:  JCDZ
  def self.fare_options
    {
      'Tango' => %w{N G P E A},
      'Tango/Flex' => %w{T L K},
      'Flex' => %w{M U H Q V W S},
      'Latitude' => %w{Y B},
      'Premium Economy' => %w{O}
    }
  end
end
