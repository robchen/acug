class Lmu < ActiveRecord::Base
  has_one :upgrade, as: :upgradeable
  accepts_nested_attributes_for :upgrade

  CURRENCY = {
    cad: 0,
    usd: 1,
    gbp: 2,
    eur: 3
  }

  def currency
    CURRENCY.key(read_attribute(:currency_id))
  end

  def currency=(s)
    write_attribute(:currency_id, CURRENCY[s])
  end
end
