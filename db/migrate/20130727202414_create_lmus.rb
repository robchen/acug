class CreateLmus < ActiveRecord::Migration
  def change
    create_table :lmus do |t|
      t.float :price
      t.integer :currency_id
      t.boolean :purchased

      t.timestamps
    end
  end
end
