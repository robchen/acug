class CreateFlights < ActiveRecord::Migration
  def change
    create_table :flights do |t|
      t.date :date
      t.integer :number
      t.integer :airplane_id
      t.integer :from_id
      t.integer :to_id
      t.string :fare

      t.timestamps
    end
  end
end
