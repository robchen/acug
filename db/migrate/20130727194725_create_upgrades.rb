class CreateUpgrades < ActiveRecord::Migration
  def change
    create_table :upgrades do |t|
      t.integer :status_id
      t.integer :cabin_id
      t.integer :flight_id

      t.references :upgradeable, polymorphic: true

      t.timestamps
    end
  end
end
