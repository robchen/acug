class CreateAirplanes < ActiveRecord::Migration
  def change
    create_table :airplanes do |t|
      t.string :code
      t.string :name

      t.timestamps
    end
  end
end
