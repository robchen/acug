# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20130825032559) do

  create_table "airplanes", force: true do |t|
    t.string   "code"
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "cities", force: true do |t|
    t.string   "code"
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "flights", force: true do |t|
    t.date     "date"
    t.integer  "number"
    t.integer  "airplane_id"
    t.integer  "from_id"
    t.integer  "to_id"
    t.string   "fare"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "lmus", force: true do |t|
    t.float    "price"
    t.integer  "currency_id"
    t.boolean  "purchased"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "upgrades", force: true do |t|
    t.integer  "status_id"
    t.integer  "cabin_id"
    t.integer  "flight_id"
    t.integer  "upgradeable_id"
    t.string   "upgradeable_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", force: true do |t|
    t.string   "username"
    t.string   "password"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
