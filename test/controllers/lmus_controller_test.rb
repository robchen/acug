require 'test_helper'

class LmusControllerTest < ActionController::TestCase
  setup do
    @lmu = lmus(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:lmus)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create lmu" do
    assert_difference('Lmu.count') do
      post :create, lmu: { currency_id: @lmu.currency_id, price: @lmu.price, purchased: @lmu.purchased, upgrade_id: @lmu.upgrade_id }
    end

    assert_redirected_to lmu_path(assigns(:lmu))
  end

  test "should show lmu" do
    get :show, id: @lmu
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @lmu
    assert_response :success
  end

  test "should update lmu" do
    patch :update, id: @lmu, lmu: { currency_id: @lmu.currency_id, price: @lmu.price, purchased: @lmu.purchased, upgrade_id: @lmu.upgrade_id }
    assert_redirected_to lmu_path(assigns(:lmu))
  end

  test "should destroy lmu" do
    assert_difference('Lmu.count', -1) do
      delete :destroy, id: @lmu
    end

    assert_redirected_to lmus_path
  end
end
